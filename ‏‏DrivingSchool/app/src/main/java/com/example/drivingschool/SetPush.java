package com.example.drivingschool;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.example.drivingschool.databinding.ActivityMainBinding;
import com.example.drivingschool.databinding.ActivitySetPushBinding;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;

import java.text.DateFormat;
import java.util.Calendar;

public class SetPush extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private ActivitySetPushBinding binding;
    private MaterialTimePicker picker;
    private Calendar calendar;
    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private boolean isTimeSelected = false;
    private boolean isDateSelected = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySetPushBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        createNotificationChannel();

        binding.btnSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });

        binding.btnSelectHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePicker();
            }
        });

        binding.btnSetAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setAlarm();

            }
        });
    }

    /*
    This function will show the date picker to select the notification date
    Input: None
    Output: None
     */
    private void showDatePicker() {
        com.example.drivingschool.DatePicker datePickerDialogFragment = new com.example.drivingschool.DatePicker();
        datePickerDialogFragment.show(getSupportFragmentManager(), "תאריך התזכורת");
    }

    /*
    This function will call after the user submit the date of the push
    Input: None
    Output: None
     */
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        isDateSelected = true;
        calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String selectedDate = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());
        binding.tvDate.setText(selectedDate);
    }

    /*
    This function will set the notification
    Input: None
    Output: None
     */
    private void setAlarm() {
        if(isDateSelected && isTimeSelected)
        {

            alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(this, AlarmReceiver.class);

            pendingIntent = PendingIntent.getBroadcast(this, 0,intent, 0);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

            Toast.makeText(this, "התזכורת נקבעה בהצלחה",Toast.LENGTH_SHORT).show();

        }
        else
        {
            Toast.makeText(getBaseContext(), "חייב לבחור תאריך ושעה",Toast.LENGTH_SHORT).show();
        }
    }

    /*
    This function will show the time picker of the notification
    Input: None
    Output: None
     */
    private void showTimePicker() {
        picker = new MaterialTimePicker.Builder()
                .setTimeFormat(TimeFormat.CLOCK_24H)
                .setHour(12)
                .setMinute(0)
                .setTitleText("שעת התזכורת")
                .build();

        picker.show(getSupportFragmentManager(), "drivingSchool");

        picker.addOnPositiveButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isTimeSelected = true;

                String hour = String.format("%02d",picker.getHour());
                String min = String.format("%02d",picker.getMinute());

                binding.tvHour.setText(hour + ":" + min);

                calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, picker.getHour() - 1);
                calendar.set(Calendar.MINUTE, picker.getMinute());
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
            }
        });

    }
    /*
    This function will create the service for the notification
    Input: None
    Output: None
     */
    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            CharSequence name = "DrivingSchool";
            String description = "Driving School Push";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("drivingSchool", name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}