package com.example.drivingschool;

public class Teacher extends Person{

    private String price;
    private String teachingCity;
    private String studentsUsernameList;

    public Teacher(String username, String name, String password, String mail, String city, String address, String phone, String price, String teachingCity, String studentsUsernameList) {
        super(username, name, password, mail, city, address, phone);

        this.price = price;
        this.teachingCity = teachingCity;
        this.studentsUsernameList = studentsUsernameList;
    }

    public String getStudentsUsernameList() {
        return studentsUsernameList;
    }

    public void setStudentsUsernameList(String studentsUsernameList) {
        this.studentsUsernameList = studentsUsernameList;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTeachingCity() {
        return teachingCity;
    }

    public void setTeachingCity(String teachingCity) {
        this.teachingCity = teachingCity;
    }

}
