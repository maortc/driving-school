package com.example.drivingschool;

public class Person {
    private String username;
    private String name;
    private String password;
    private String mail;
    private String city;
    private String address;
    private String phone;

    //  C'tor
    public Person(String username, String name, String password, String mail, String city, String address, String phone) {
        this.username = username;
        this.name = name;
        this.password = password;
        this.mail = mail;
        this.city = city;
        this.address = address;
        this.phone = phone;
    }

    // Getters
    public String getUsername() {
        return username;
    }
    public String getName() {
        return name;
    }
    public String getPassword() {
        return password;
    }
    public String getMail() {
        return mail;
    }
    public String getCity() {
        return city;
    }
    public String getAddress() {
        return address;
    }
    public String getPhone(){ return phone;}

    // Setters
    public void setUsername(String username) {
        this.username = username;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setPhone(String phone) {this.phone = phone;}
}
