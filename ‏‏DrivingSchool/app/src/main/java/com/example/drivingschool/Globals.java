package com.example.drivingschool;

import android.app.Application;
import android.content.Context;

import java.util.ArrayList;

public class Globals extends Application {
    private static Globals instance;
    private static String loggedUsername;

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
    }
    /*
    This function will set the username of the logged user
    Input: Username of the logged user
    Output: None
     */
    public static void setLoggedUsername(String username)
    {
        loggedUsername = username;
    }
    /*
    This function will return the username of the logged user
    Input: None
    Output: Username of the logged user
     */
    public static String getLoggedUsername()
    {
        return loggedUsername;
    }

    /*
    This function will return instance of the class
    Input: None
    Output: instance of the class
     */
    public static Globals getInstance() {
        return instance;
    }

    /*
    This function will return context of the class
    Input: None
    Output: context of the class
     */
    public static Context getContext(){
        return instance;
    }
}
