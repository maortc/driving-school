package com.example.drivingschool;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;


public class TeacherInfo extends AppCompatActivity {
    String username, name, city, phone, mail, price;
    TextView tvName, tvCity, tvPhone, tvMail, tvPrice;
    ImageView ivImage;
    HelperDB db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_info);

        // Init vars
        db = new HelperDB(this);
        tvName = findViewById(R.id.tvNameInfo);
        tvCity = findViewById(R.id.tvCityInfo);
        tvPhone = findViewById(R.id.tvPhoneInfo);
        tvMail = findViewById(R.id.tvEmailInfo);
        tvPrice = findViewById(R.id.tvPriceInfo);
        ivImage = findViewById(R.id.ivImageInfo);
        Bundle extras = getIntent().getExtras();
        username = extras.getString("username");
        name = extras.getString("name");
        city = extras.getString("city");
        phone = extras.getString("phone");
        mail = extras.getString("mail");
        price = extras.getString("price");

        tvName.setText(name);
        tvPrice.setText(price + "₪");
        tvMail.setText(mail);
        tvCity.setText(city);
        tvPhone.setText(phone);

        setImage(username, ivImage);

    }

    /*
   This function will receive the image of the student from the firebase storage and set in the imgView
    Input: Student of the picture, imageView
    Output: None
    */
    public void setImage(String username, ImageView imageView){
        // we will get the default FirebaseStorage instance
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();

        // we will get a StorageReference for the database root node

        String imgPath = "picture/" + username;
        StorageReference storageRef = firebaseStorage.getInstance().getReference();
        storageRef.child(imgPath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(@NonNull Uri uri) {
                Picasso.with(TeacherInfo.this).load(uri).into(imageView);
            }
        });
    }
    /*
    This function will set the teacher for the student
    Input: None
    Output: None
     */
    public void updateTeacher(View view) {
        db.updateTeacherUsername(Globals.getInstance().getLoggedUsername(), username);
        Intent intent = new Intent(this, StudentMenu.class);
        startActivity(intent);
    }
}