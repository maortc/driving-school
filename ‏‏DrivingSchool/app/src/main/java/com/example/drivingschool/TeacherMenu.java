package com.example.drivingschool;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class TeacherMenu extends AppCompatActivity {
    Dialog dialogChangePrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_menu);
        dialogChangePrice = new Dialog(TeacherMenu.this);

    }

    /*
    This function will logout and send the user to the logging screen
    Input: None
    Output: None
     */
    public void logout(View view) {
        Globals.getInstance().setLoggedUsername("");
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /*
    This function will change the lesson price of the teacher
    Input: None
    Output: None
    */
    public void changePrice(View view) {
        Intent intent = new Intent(this, ChangePrice.class);
        startActivity(intent);
    }

    /*
    This function will open screen with student list of the teacher and when the user click its
    will open google maps to navigate to the student address
    Input: None
    Output: None
     */
    public void navigateToStudent(View view) {
        Intent intent = new Intent(this, StudentList.class);
        startActivity(intent);
    }

}