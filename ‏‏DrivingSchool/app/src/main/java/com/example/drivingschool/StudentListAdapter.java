package com.example.drivingschool;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.List;

public class StudentListAdapter  extends ArrayAdapter<Student> {
    List<Student> studentsList;
    Context context;
    int resource;


    public StudentListAdapter(Context context, int resource, List<Student> studentsList) {
        super(context, resource, studentsList);
        this.context = context;
        this.resource = resource;
        this.studentsList = studentsList;

    }
    //this will return the ListView Item as a View
    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        //we need to get the view of the xml for our list item
        //And for this we need a layoutinflater
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        //getting the view
        View view = layoutInflater.inflate(resource, null, false);

        //getting the view elements of the list from the view
        ImageView imageView = view.findViewById(R.id.imageView);
        TextView textViewName = view.findViewById(R.id.textViewName);


        //getting the teacher of the specified position
        Student student = studentsList.get(position);

        //adding values to the list item
        setImage(student, imageView);
        textViewName.setText(student.getName());

        //finally returning the view
        return view;
    }

    /*
    This function will receive the image of the student from the firebase storage and set in the imgView
     Input: Student of the picture, imageView
     Output: None
     */
    public void setImage(Student student, ImageView imageView){
        // we will get the default FirebaseStorage instance
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();

        // we will get a StorageReference for the database root node

        String imgPath = "picture/" + student.getUsername();
        StorageReference storageRef = firebaseStorage.getInstance().getReference();
        storageRef.child(imgPath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(@NonNull Uri uri) {
                Picasso.with(context).load(uri).into(imageView);
            }
        });
    }
}
