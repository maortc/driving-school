package com.example.drivingschool;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.textclassifier.ConversationActions;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    Dialog dialogResetPassword;
    Dialog dialogRegisterType;
    private HelperDB db;
    private EditText username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new HelperDB(this);
        dialogResetPassword = new Dialog(MainActivity.this);
        dialogRegisterType = new Dialog(MainActivity.this);
        username = findViewById(R.id.etUsername);
        password = findViewById(R.id.etPassword);

    }

    /*
    This function will send the user to the RegisterStudent Activity
    Input: None
    Output: None
     */
    public void IntentRegisterStudent(View view)
    {
        Intent intent = new Intent(this, RegisterStudent.class);
        startActivity(intent);
    }
    /*
    This function will send the user to the RegisterTeacher Activity
    Input: None
    Output: None
     */
    public void IntentRegisterTeacher(View view)
    {
        Intent intent = new Intent(this, RegisterTeacher.class);
        startActivity(intent);
    }

    /*
    This function will open dialog for the user to select if he want to register as a teacher or student
    Input: None
    Output: None
     */
    public void register(View view)
    {
        dialogRegisterType.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT)); // Set dialog background to transparent
        dialogRegisterType.setCancelable(true); // The user will be able to cancel the dialog by clicking anywhere outside the dialog
        dialogRegisterType.setContentView(R.layout.select_person_type_dialog); // set the layout of the custom dialog
        dialogRegisterType.show();
    }


    /*
    This function will login the user
    Input: None
    Output: None
     */
    public void signIn(View view) {

         if(allFieldsValid()){
            HelperDB helperDB = new HelperDB(this);
            int status = helperDB.checkUsernameAndPassword(username.getText().toString(), password.getText().toString());
            if(status == 0)
            {
                Toast.makeText(this, "שם המשתמש או הסיסמה לא נכונים", Toast.LENGTH_SHORT).show();
            }
            else if(status == 1) // Username and password are valid and its student
            {
                Globals.getInstance().setLoggedUsername(username.getText().toString());
                if(db.hasTeacher(username.getText().toString()))
                {
                    Intent intent = new Intent(this, StudentMenu.class);
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(this, TeacherList.class);
                    startActivity(intent);
                }

            }
            else // Username and password are valid and its Teacher
            {
                Globals.getInstance().setLoggedUsername(username.getText().toString());
                Intent intent = new Intent(this, TeacherMenu.class);
                startActivity(intent);

            }

        }
        else
        {
            Toast.makeText(this, "אתה חייב למלא את כל השדות", Toast.LENGTH_SHORT).show();
        }


    }
    /*
    This function will check if the user fill all the fields
    Input: None
    Output: True if he filled else false
     */
    public boolean allFieldsValid()
    {
        if(username.length() == 0 || password.length() == 0)
            return false;
        return true;
    }
}