package com.example.drivingschool;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StudentList extends AppCompatActivity {


    //a List of type hero for holding list items
    List<Student> studentsList;

    //the listview
    ListView listView;

    HelperDB helperDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);

        //initializing objects
        helperDB = new HelperDB(this);
        studentsList = new ArrayList<>();
        listView = (ListView) findViewById(R.id.listView);


        //adding values to the list
        studentsList = helperDB.getStudentsOfTeacher(Globals.getLoggedUsername());

        //creating the adapter
        StudentListAdapter adapter = new StudentListAdapter(this, R.layout.student_list, studentsList);

        //attaching adapter to the listview
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Student student = (Student) listView.getItemAtPosition(position);
                openMaps(student);

            }
        });

    }

    /*
    This function will open the google maps and navigate to the student address
    Input: Student to navigate
    Output: None
     */
    public void openMaps(Student student) {
        Geocoder geocoder = new Geocoder(this);
        List<Address> addresses;
        double latitude =0.0;
        double longitude = 0.0;
        try {
            String fullAddress = student.getAddress() + ", " + student.getCity();
            addresses = geocoder.getFromLocationName(fullAddress +
                    "", 1);
            if(addresses.size() > 0) {
                latitude= addresses.get(0).getLatitude();
                longitude= addresses.get(0).getLongitude();
            }
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse(String.format("http://maps.google.com/maps?daddr=%f,%f" , latitude, longitude)));
            startActivity(intent);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}