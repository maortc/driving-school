package com.example.drivingschool;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ChangePrice extends AppCompatActivity {
    HelperDB helperDB;
    EditText newPrice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_price);
        helperDB = new HelperDB(this);
        newPrice = findViewById(R.id.etNewPrice);
    }

    /*
    This function will change the lesson price of the teacher
    Input: None
    Output: None
     */
    public void changeLessonPrice(View view) {
        helperDB.changeTeacherPrice(Globals.getLoggedUsername(), newPrice.getText().toString());
        Intent intent = new Intent(this, TeacherMenu.class);
        startActivity(intent);

    }
}