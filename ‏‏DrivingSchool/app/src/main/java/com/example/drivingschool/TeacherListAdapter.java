package com.example.drivingschool;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TeacherListAdapter extends ArrayAdapter<Teacher> {
    List<Teacher> teacherList;
    Context context;
    int resource;


    public TeacherListAdapter(Context context, int resource, List<Teacher> teacherList) {
        super(context, resource, teacherList);
        this.context = context;
        this.resource = resource;
        this.teacherList = teacherList;

    }


    //this will return the ListView Item as a View
    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        //we need to get the view of the xml for our list item
        //And for this we need a layoutinflater
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        //getting the view
        View view = layoutInflater.inflate(resource, null, false);

        //getting the view elements of the list from the view
        ImageView imageView = view.findViewById(R.id.imageView);
        TextView textViewName = view.findViewById(R.id.textViewName);
        TextView textViewPrice = view.findViewById(R.id.textViewPrice);


        //getting the teacher of the specified position
        Teacher teacher = teacherList.get(position);

        //adding values to the list item
        setImage(teacher, imageView);
        textViewName.setText(teacher.getName());
        textViewPrice.setText(teacher.getPrice());

        //finally returning the view
        return view;
    }

    public void setImage(Teacher teacher, ImageView imageView){
        // we will get the default FirebaseStorage instance
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();

        // we will get a StorageReference for the database root node

        String imgPath = "picture/" + teacher.getUsername();
        StorageReference storageRef = firebaseStorage.getInstance().getReference();
        storageRef.child(imgPath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(@NonNull Uri uri) {
                //imageView.setImageURI(uri);
                Picasso.with(context).load(uri).into(imageView);
            }
        });
    }
}
