package com.example.drivingschool;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class HelperDB extends SQLiteOpenHelper {

    // Database name
    private static final String DB_NAME = "DrivingSchool";

    // Tables name
    private static final String STUDENTS_TABLE_NAME = "Students";
    private static final String TEACHERS_TABLE_NAME = "Teachers";

    // Columns name
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_MAIL = "mail";
    private static final String COLUMN_CITY = "city";
    private static final String COLUMN_ADDRESS = "address";
    private static final String COLUMN_PHONE = "phone";
    private static final String COLUMN_TEORIA = "teoria";
    private static final String COLUMN_TEACHER_USERNAME = "teacherUsername";
    private static final String COLUMN_HAVE_TEACHER = "haveTeacher";
    private static final String COLUMN_TEACHING_CITY = "teachingCity";
    private static final String COLUMN_PRICE = "price";
    private static final String COLUMN_STUDENTS_USERNAME_LIST = "studentsUsernameList";


    // C'tor
    public HelperDB(@Nullable Context context){
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create teachers table if not exists
        String str = "CREATE TABLE IF NOT EXISTS  " + TEACHERS_TABLE_NAME + " (";
        str += COLUMN_USERNAME + " TEXT, ";
        str += COLUMN_NAME + " TEXT, ";
        str += COLUMN_PASSWORD + " TEXT, ";
        str += COLUMN_MAIL + " TEXT, ";
        str += COLUMN_CITY + " TEXT, ";
        str += COLUMN_PHONE + " TEXT, ";
        str += COLUMN_PRICE + " TEXT, ";
        str += COLUMN_TEACHING_CITY + " TEXT, ";
        str += COLUMN_STUDENTS_USERNAME_LIST + " TEXT, ";
        str += COLUMN_ADDRESS + " TEXT);";
        db.execSQL(str);

        // Create student table if not exists
        str = "CREATE TABLE IF NOT EXISTS  " + STUDENTS_TABLE_NAME + " (";
        str += COLUMN_USERNAME + " TEXT, ";
        str += COLUMN_NAME + " TEXT, ";
        str += COLUMN_PASSWORD + " TEXT, ";
        str += COLUMN_MAIL + " TEXT, ";
        str += COLUMN_CITY + " TEXT, ";
        str += COLUMN_TEORIA + " TEXT, ";
        str += COLUMN_PHONE + " TEXT, ";
        str += COLUMN_HAVE_TEACHER + " TEXT, ";
        str += COLUMN_TEACHER_USERNAME + " TEXT, ";
        str += COLUMN_ADDRESS + " TEXT);";
        db.execSQL(str);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { }
    /*
    This function will insert new student to the database
    Input: Student to insert
    Output: None
     */
    public void insertNewStudent(Student student){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_USERNAME, student.getUsername());
        values.put(COLUMN_NAME, student.getName());
        values.put(COLUMN_PASSWORD, student.getPassword());
        values.put(COLUMN_MAIL, student.getMail());
        values.put(COLUMN_CITY, student.getCity());
        values.put(COLUMN_ADDRESS, student.getAddress());
        values.put(COLUMN_PHONE, student.getPhone());
        values.put(COLUMN_TEORIA, student.isHasTeoria());
        values.put(COLUMN_HAVE_TEACHER, "0");
        db.insert(STUDENTS_TABLE_NAME, null, values);
    }

    /*
   This function will insert new Teacher to the database
   Input: Student to insert
   Output: None
    */
    public void insertNewTeacher(Teacher teacher){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_USERNAME, teacher.getUsername());
        values.put(COLUMN_NAME, teacher.getName());
        values.put(COLUMN_PASSWORD, teacher.getPassword());
        values.put(COLUMN_MAIL, teacher.getMail());
        values.put(COLUMN_CITY, teacher.getCity());
        values.put(COLUMN_ADDRESS, teacher.getAddress());
        values.put(COLUMN_PHONE, teacher.getPhone());
        values.put(COLUMN_PRICE, teacher.getPrice());
        values.put(COLUMN_TEACHING_CITY, teacher.getTeachingCity());
        values.put(COLUMN_STUDENTS_USERNAME_LIST, "");
        db.insert(TEACHERS_TABLE_NAME, null, values);
    }

    /*
    This function will check if the username already exists in the database
    Input: username to check, In which table to check
    Output: true if exists else false
     */
    public boolean isUsernameExists(String value)
    {
        // Check if the username exists in the students table
        String queryStudent = "SELECT * FROM " + STUDENTS_TABLE_NAME + " WHERE " + COLUMN_USERNAME + " = ?";
        String[] whereArgsStudent = {value};
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursorStudent = db.rawQuery(queryStudent, whereArgsStudent);
        int count = cursorStudent.getCount();
        cursorStudent.close();

        // Check if the username exists in the teachers table
        String queryTeachers = "SELECT * FROM " + TEACHERS_TABLE_NAME + " WHERE " + COLUMN_USERNAME + " = ?";
        String[] whereArgsTeachers = {value};
        Cursor cursor = db.rawQuery(queryTeachers, whereArgsTeachers);
        count += cursor.getCount();
        cursor.close();

        return count >= 1;
    }

    /*
    This function will check if the mail already exists in the database
    Input: mail to check, In which table to check
    Output: true if exists else false
     */
    public boolean isMailExists(String value)
    {
        // Check if the mail exists in the students table
        String queryStudent = "SELECT * FROM " + STUDENTS_TABLE_NAME + " WHERE " + COLUMN_MAIL + " = ?";
        String[] whereArgsStudent = {value};
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursorStudent = db.rawQuery(queryStudent, whereArgsStudent);
        int count = cursorStudent.getCount();
        cursorStudent.close();

        // Check if the mail exists in the teachers table
        String queryTeachers = "SELECT * FROM " + TEACHERS_TABLE_NAME + " WHERE " + COLUMN_MAIL + " = ?";
        String[] whereArgsTeachers = {value};
        Cursor cursor = db.rawQuery(queryTeachers, whereArgsTeachers);
        count += cursor.getCount();
        cursor.close();

        return count >= 1;
    }

    /*
    This function will check if the phone already exists in the database
    Input: phone to check, In which table to check
    Output: true if exists else false
     */
    public boolean isPhoneExists(String value)
    {
        // Check if the phone exists in the students table
        String queryStudent = "SELECT * FROM " + STUDENTS_TABLE_NAME + " WHERE " + COLUMN_PHONE + " = ?";
        String[] whereArgsStudent = {value};
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursorStudent = db.rawQuery(queryStudent, whereArgsStudent);
        int count = cursorStudent.getCount();
        cursorStudent.close();

        // Check if the phone exists in the teachers table
        String queryTeachers = "SELECT * FROM " + TEACHERS_TABLE_NAME + " WHERE " + COLUMN_PHONE + " = ?";
        String[] whereArgsTeachers = {value};
        Cursor cursor = db.rawQuery(queryTeachers, whereArgsTeachers);
        count += cursor.getCount();
        cursor.close();

        return count >= 1;
    }

    public ArrayList<Teacher> getTeachers() {
        String username, name, password, mail, city, address, phone, price, teachingCity, studentsList;
        ArrayList<Teacher> arrayList=new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT * FROM " + TEACHERS_TABLE_NAME, null );
        res.moveToFirst();

        if (res != null)
        {
            while(res.isAfterLast() == false){
                username = res.getString(res.getColumnIndex(COLUMN_USERNAME));
                name = res.getString(res.getColumnIndex(COLUMN_NAME));
                password = res.getString(res.getColumnIndex(COLUMN_PASSWORD));
                mail = res.getString(res.getColumnIndex(COLUMN_MAIL));
                city = res.getString(res.getColumnIndex(COLUMN_CITY));
                address = res.getString(res.getColumnIndex(COLUMN_ADDRESS));
                phone = res.getString(res.getColumnIndex(COLUMN_PHONE));
                price = res.getString(res.getColumnIndex(COLUMN_PRICE));
                teachingCity = res.getString(res.getColumnIndex(COLUMN_TEACHING_CITY));
                studentsList = res.getString(res.getColumnIndex(COLUMN_STUDENTS_USERNAME_LIST));
                Teacher teacher = new Teacher(username, name, password, mail, city, address, phone, price, teachingCity, studentsList);
                arrayList.add(teacher);

                res.moveToNext();
            }
        }
        return arrayList;
    }
    /*
    Check if the username and password correct
    Input: username and password
    Output: 0 not valid, 1 valid and its students, 2 valid and its teacher
     */
    public int checkUsernameAndPassword(String username, String password)
    {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT * FROM " + STUDENTS_TABLE_NAME + " WHERE " + COLUMN_USERNAME + " = '" + username + "' AND " + COLUMN_PASSWORD + " = '" + password +"'", null );

        if(res.getCount() > 0 ) // Its student
            return 1;

        res = db.rawQuery( "SELECT * FROM " + TEACHERS_TABLE_NAME + " WHERE " + COLUMN_USERNAME + " = '" + username + "' AND " + COLUMN_PASSWORD + " = '" + password + "'", null );
        if(res.getCount() > 0) // Its teacher
            return  2;

        return 0;  // Password or username are wrong
    }

    /*
   Add the teacher username to the student database
   Input: studentUsername, teacherUsername
   Output: None
    */
    public void updateTeacherUsername(String studentUsername, String teacherUsername)
    {
        String studentsList;
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(COLUMN_TEACHER_USERNAME, teacherUsername);
        cv.put(COLUMN_HAVE_TEACHER, "1");
        db.update(STUDENTS_TABLE_NAME, cv, COLUMN_USERNAME + "= ?", new String[] {studentUsername});

        Cursor res =  db.rawQuery( "SELECT * FROM " + TEACHERS_TABLE_NAME +" WHERE " + COLUMN_USERNAME + " = '" + teacherUsername + "'" , null );
        res.moveToFirst();
        studentsList = res.getString(res.getColumnIndex(COLUMN_STUDENTS_USERNAME_LIST));
        studentsList += studentUsername + ",";
        cv = new ContentValues();
        cv.put(COLUMN_STUDENTS_USERNAME_LIST, studentsList);
        db.update(TEACHERS_TABLE_NAME, cv, COLUMN_USERNAME + "= ?", new String[] {teacherUsername});

    }

    /*
    Check if the user has teacher or not
    Input: Student username
    Output: True if has, else false
    */
    public boolean hasTeacher(String studentUsername)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT * FROM " + STUDENTS_TABLE_NAME + " WHERE " + COLUMN_USERNAME + " = '" + studentUsername + "' AND " + COLUMN_HAVE_TEACHER + " = '" + "1" +"'", null );
        if(res.getCount() > 0 ) // The student has a teacher
            return true;
        return false;
    }

    /*
    This function will remove the teacher from the student database and remove the student username
     from the student list of the teacher in the database
     Input: None
     Output: None
     */
    public void removeTeacherOfStudent(String studentUsername)
    {
        String studentsList;
        String teacherUsername;

        // Get the teacher username of the student
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT * FROM " + STUDENTS_TABLE_NAME +" WHERE " + COLUMN_USERNAME + " = '" + studentUsername + "'" , null );
        res.moveToFirst();
        teacherUsername = res.getString(res.getColumnIndex(COLUMN_TEACHER_USERNAME));

        //Remove the teacher username and change the haveTeacher status to false
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_TEACHER_USERNAME, "");
        cv.put(COLUMN_HAVE_TEACHER, "0");
        db.update(STUDENTS_TABLE_NAME, cv, COLUMN_USERNAME + "= ?", new String[] {studentUsername});

        // Remove the student username from the student list of the teacher
        Cursor res1 =  db.rawQuery( "SELECT * FROM " + TEACHERS_TABLE_NAME +" WHERE " + COLUMN_USERNAME + " = '" + teacherUsername + "'" , null );
        res1.moveToFirst();
        studentsList = res1.getString(res1.getColumnIndex(COLUMN_STUDENTS_USERNAME_LIST));
        studentsList = studentsList.replace(("," + studentUsername), "");
        studentsList = studentsList.replace((studentUsername), ""); //If the student username is first in the list

        // Update the student list of the teacher in the database
        cv = new ContentValues();
        cv.put(COLUMN_STUDENTS_USERNAME_LIST, studentsList);
        db.update(TEACHERS_TABLE_NAME, cv, COLUMN_USERNAME + "= ?", new String[] {teacherUsername});

    }


    /*
    This function will return all the student of the teacher
    Input: Teacher of the students
    Output: List of all the teacher's students
     */
    public ArrayList<Student> getStudentsOfTeacher(String teacher) {
        String username, name, password, mail, city, address, phone, teacherUsername;
        String studentsList;
        boolean hasTeoria, haveTeacher;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res1 =  db.rawQuery( "SELECT * FROM " + TEACHERS_TABLE_NAME +" WHERE " + COLUMN_USERNAME + " = '" + teacher + "'" , null );
        res1.moveToFirst();
        studentsList = res1.getString(res1.getColumnIndex(COLUMN_STUDENTS_USERNAME_LIST));

        ArrayList<Student> arrayList=new ArrayList<>();


        Cursor res =  db.rawQuery( "SELECT * FROM " + STUDENTS_TABLE_NAME, null );
        res.moveToFirst();


        if (res != null)
        {
            while(res.isAfterLast() == false){
                // Check if the student username contains in the student list of the teacher
                if(studentsList.contains(res.getString(res.getColumnIndex(COLUMN_USERNAME))))
                {
                    username = res.getString(res.getColumnIndex(COLUMN_USERNAME));
                    name = res.getString(res.getColumnIndex(COLUMN_NAME));
                    password = res.getString(res.getColumnIndex(COLUMN_PASSWORD));
                    mail = res.getString(res.getColumnIndex(COLUMN_MAIL));
                    city = res.getString(res.getColumnIndex(COLUMN_CITY));
                    address = res.getString(res.getColumnIndex(COLUMN_ADDRESS));
                    phone = res.getString(res.getColumnIndex(COLUMN_PHONE));
                    teacherUsername = res.getString(res.getColumnIndex(COLUMN_TEACHER_USERNAME));
                    hasTeoria = true; // not important
                    haveTeacher = true;
                    Student student = new Student(username, name, password, mail, city, address, phone, hasTeoria, haveTeacher, teacherUsername);
                    arrayList.add(student);
                }
                res.moveToNext();
            }
        }
        return arrayList;
    }
    /*
    This function will change the price of the lesson
    Input: Teacher username who wants to change price, new price of lesson
    Output: None
     */
    public void changeTeacherPrice(String teacherUsername, String newPrice)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(COLUMN_PRICE, newPrice);
        db.update(TEACHERS_TABLE_NAME, cv, COLUMN_USERNAME + "= ?", new String[] {teacherUsername});
    }

}