package com.example.drivingschool;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class TeacherList extends AppCompatActivity {

    List<Teacher> teacherList;
    ListView listView;
    HelperDB helperDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_list);

        //initializing objects
        helperDB = new HelperDB(this);
        teacherList = new ArrayList<>();
        listView = (ListView) findViewById(R.id.listView);


        //adding values to the list
        teacherList = helperDB.getTeachers();

        //creating the adapter
        TeacherListAdapter adapter = new TeacherListAdapter(this, R.layout.teacher_list, teacherList);

        //attaching adapter to the listview
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new Intent(TeacherList.this, TeacherInfo.class);
                Teacher teacher = (Teacher) listView.getItemAtPosition(position);

                intent.putExtra("username", teacher.getUsername());
                intent.putExtra("name", teacher.getName());
                intent.putExtra("price", teacher.getPrice());
                intent.putExtra("city", teacher.getCity());
                intent.putExtra("mail", teacher.getMail());
                intent.putExtra("phone",teacher.getPhone());

                startActivity(intent);
            }
        });

    }


}