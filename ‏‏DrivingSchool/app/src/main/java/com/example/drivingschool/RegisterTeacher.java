package com.example.drivingschool;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class RegisterTeacher extends AppCompatActivity {
    private EditText etName, etUsername, etPassword, etMail, etCity, etAddress, etTeachingCity, etPrice, etPhone;
    private static final int CAMERA_PERM_CODE = 101;
    private static final int CAMERA_REQUEST_CODE = 102;
    private static final int GALLERY_REQUEST_CODE = 105;

    private Button cameraBtn, galleryBtn, startTeachingBtn, endTeachingBtn;
    private StorageReference storageReference;
    private HelperDB db;
    private String imgPath = "";
    private String selectedImagePath = "";
    private Uri imgUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_teacher);

        // Init vars
        etName = findViewById(R.id.etNameRegisterT);
        etUsername = findViewById(R.id.etUsernameRegisterT);
        etPassword = findViewById(R.id.etPasswordRegisterT);
        etMail = findViewById(R.id.etMailRegisterT);
        etCity = findViewById(R.id.etCityRegisterT);
        etAddress = findViewById(R.id.etAddressRegisterT);
        etPhone = findViewById(R.id.etPhoneRegisterT);
        etPrice = findViewById(R.id.etPriceRegisterT);
        etTeachingCity = findViewById(R.id.etStudyCityRegisterT);
        cameraBtn = findViewById(R.id.CameraBtnT);
        galleryBtn = findViewById(R.id.galleryBtnT);
        storageReference = FirebaseStorage.getInstance().getReference();

        db = new HelperDB(this);

        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askCameraPermission();
            }
        });

        galleryBtn.setOnClickListener((view) -> {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(gallery, GALLERY_REQUEST_CODE);
        });

    }

    /*
   Check if the app have permissions to access the Camera
   input: None
   Output: None
    */
    private void askCameraPermission(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, CAMERA_PERM_CODE);
        else{
            dispatchTakePictureIntent();
        }
    }

    /*
   This function will open the camera and handle with the photo
   Input: None
   Output: None
    */
    private void dispatchTakePictureIntent() {
        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
        startActivityForResult(intent, CAMERA_REQUEST_CODE);
    }

    /*
   This function will create a file from the photo
   Input: None
   Output: Path of the photo
    */
    public Uri setImageUri() {
        File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".png");
        Uri imgUri = FileProvider.getUriForFile(RegisterTeacher.this,  "com.example.DrivingSchool.fileprovider", file);
        this.imgPath = file.getAbsolutePath();
        return imgUri;
    }


    /*
   This function will handle with camera or gallery request and set all the vars before upload the image to the firebase
   Input: Type of image, Image to upload
   Output: None
    */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == CAMERA_REQUEST_CODE){
            selectedImagePath = this.imgPath;
            imgUri = Uri.fromFile(new File(selectedImagePath));
            try {
                imgUri = rotateImage(imgUri); // Fix emulator bug
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        else if(requestCode == GALLERY_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK) {
                imgUri = data.getData();
            }
        }
        else{
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    /*
    This function will upload the image to the firebase
    Input: Name of the file, Image to upload
    Output: None
     */
    private void uploadImageToFirebase(String name, Uri contentUri) throws IOException {
        final StorageReference image = storageReference.child("picture/" + name);
        image.putFile(contentUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                image.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                    }
                });

            }
        }).addOnFailureListener((e) -> {

        });
    }

    /*
    This function will add the teacher to the database
    Input: None
    Output: None
     */
    public void signUpTeacher(View view) throws IOException {
            if(allFieldsFilled())
            {
                // Upload user to database
                if(allFieldAreCorrect())
                {
                    Teacher teacher = new Teacher(etUsername.getText().toString(), etName.getText().toString(), etPassword.getText().toString(),
                            etMail.getText().toString(), etCity.getText().toString(), etAddress.getText().toString(), etPhone.getText().toString(),
                            etPrice.getText().toString(), etTeachingCity.getText().toString(), "");

                   db.insertNewTeacher(teacher);

                    // Upload user picture to database
                    String imageFileName = etUsername.getText().toString();
                    uploadImageToFirebase(imageFileName, imgUri);

                    Globals.getInstance().setLoggedUsername(etUsername.getText().toString());
                    Intent intent = new Intent(this, TeacherMenu.class);
                    startActivity(intent);
                }
            }
            else
            {
                Toast.makeText(this,"מלא את כל השדות", Toast.LENGTH_LONG).show();
            }
    }

    /*
    This function will check if the user fill all the fields
    Input: None
    Output: True if all the fields filled else False
     */
    public boolean allFieldsFilled()
    {
        if(etName.getText().toString() == null || etUsername.getText().toString() == null ||
                etMail.getText().toString() == null || etCity.getText().toString() == null || etAddress.getText().toString() == null ||
                etPassword.getText().toString() == null || etPhone.getText().toString() == null ||etPrice.getText().toString() == null || etTeachingCity.getText().toString() == null || imgUri == null )
            return false;
        else
            return true;
    }

    /*
  This function will check if the user enter a valid data to the fields
  Input: None
  Output: True if all the fields correct, else False
   */
    public boolean allFieldAreCorrect()
    {
        // Check if username exist in the database
        if(db.isUsernameExists(etUsername.getText().toString()))
        {
            Toast.makeText(this, "שם המשתמש קיים במערכת", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Check if the email format is valid
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(etMail.getText().toString()).matches())
        {
            Toast.makeText(this, "אנא הזמן מייל תקין", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Check if mail exist in the database
        if(db.isMailExists(etMail.getText().toString()))
        {
            Toast.makeText(this, "המייל קיים במערכת", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Check if username and password equal
        if(etUsername.getText().toString().equals(etPassword.getText().toString())){
            Toast.makeText(this, "שם המשתמש והסיסמה אינם יכולים להיות זהים", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Check if the phone format is valid
        if(etPhone.getText().toString().length() != 10)
        {
            Toast.makeText(this, "פלאפון לא תקין", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Check if the phone already exists
        if(db.isPhoneExists(etPhone.getText().toString())) {
            Toast.makeText(this, "הפלאפון קיים במערכת", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    /*
    This function will rotate the image by 90 to fix bug in the camera
    Input: Image to rotate
    Output: The image of the rotate
     */
    public Uri rotateImage(Uri img) throws IOException {
        int angle = 270;
        Bitmap source = null;
        ContentResolver contentResolver = getContentResolver();

        source = MediaStore.Images.Media.getBitmap(contentResolver, img);

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        source =  Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        source.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getApplicationContext().getContentResolver(), source, "val", null);
        Uri uri = Uri.parse(path);
        return uri;
    }

}