package com.example.drivingschool;
import android.database.sqlite.SQLiteDatabase;

public class Student extends Person{
    private boolean hasTeoria;
    private boolean haveTeacher;
    private String teacherUsername;

    // C'tor
    public Student(String username, String name, String password, String mail, String city, String address, String phone, boolean hasTeoria, boolean haveTeacher, String teacherUsername) {
        super(username, name, password, mail, city, address, phone);
        this.hasTeoria = hasTeoria;
        this.haveTeacher = haveTeacher;
        this.teacherUsername= teacherUsername;
    }


    // Getters
    public boolean isHasTeoria() {
        return hasTeoria;
    }
    public String getTeacherUsername() {
        return teacherUsername;
    }
    public boolean isHaveTeacher() {
        return haveTeacher;
    }

    // Setters
    public void setHasTeoria(boolean hasTeoria) {
        this.hasTeoria = hasTeoria;
    }
    public void setHaveTeacher(boolean haveTeacher) {
        this.haveTeacher = haveTeacher;
    }
    public void setTeacherUsername(String teacherUsername) { this.teacherUsername = teacherUsername; }

}
