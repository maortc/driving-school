package com.example.drivingschool;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import java.io.IOException;
import java.util.List;

public class StudentMenu extends AppCompatActivity {
    private HelperDB db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_menu);
        db = new HelperDB(this);
    }

    /*
    This function will change the teacher of the student
    Input: None
    Output: None
     */
    public void changeTeacher(View view) {
        db.removeTeacherOfStudent(Globals.getLoggedUsername());
        Intent intent = new Intent(this, TeacherList.class);
        startActivity(intent);
    }

    /*
    This function will logout and send the user to the logging screen
    Input: None
    Output: None
     */
    public void logout(View view) {
        Globals.getInstance().setLoggedUsername("");
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /*
    This function will send the user to activity for add remind to a lesson
    Input: None
    Output: None
     */
    public void addNotification(View view) {
        Intent intent = new Intent(this, SetPush.class);
        startActivity(intent);
    }
}